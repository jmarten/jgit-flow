# JGit-Flow

**Current Version: 0.16**

JGit-Flow is a JAVA implementation of Vincent Driessen's [branching model](http://nvie.com/git-model "original
blog post").

It is based on Vincent Driessen's excellent command line tool [gitflow](https://github.com/nvie/gitflow) and is built on top of [JGit](http://eclipse.org/jgit/)

Currently it is only implemented as a JAVA library, but we will soon be adding JGit command line integration.

## Read The Wiki
For more information and usage guide, [see the wiki](https://bitbucket.org/atlassian/jgit-flow/wiki)
